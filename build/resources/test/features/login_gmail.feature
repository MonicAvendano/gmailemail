#language:es

Característica: el usuario quiere ingresar a el correo electronico en gmail

  Esquema del escenario: se debe permitir el ingreso al correo electronico de gmail
    Dado que el usuario se encuentra en la pagina de gmail
    Cuando el ingresa el correo "<correo>" y la constraseña "<contrasena>"
    Entonces el puede visualizar la bandeja del correo

    Ejemplos:

      | correo                     | contrasena  |
      | pruebaautationqa@gmail.com | Pruebas1234 |