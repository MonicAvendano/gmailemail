package stepdefinitions;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import interactions.AbrirElNavegador;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;


public class LoginGmailStepDefinition {

    @Dado("^que el (.*) se encuentra en la pagina de gmail$")
    public void queElUsuarioSeEncuentraEnLaPaginaDeGmail(String nombreActor)  {
        theActorCalled(nombreActor).attemptsTo(
                AbrirElNavegador.enPaginaDeGoogle()
        );
    }


    @Cuando("^el ingresa el correo \"([^\"]*)\" y la constraseña \"([^\"]*)\"$")
    public void elIngresaElCorreoYLaConstraseña(String arg1, String arg2)  {
        ;
    }

    @Entonces("^el puede visualizar la bandeja del correo$")
    public void elPuedeVisualizarLaBandejaDelCorreo() {
        ;
    }


}
